import { Request, Response } from "express";
import { EntityManager } from "typeorm";

export type ContextType = {
  em: EntityManager;
  req: Request & { session: {userId: number}};
  // req: Request & {session: {userId: number}};
  res: Response;
};

export type EntityKey<T,U> = Exclude<Extract<keyof T, keyof U>, "id">;
export type RelationKey<T,U> = keyof Omit<T,Extract<keyof T, keyof U>>;