import "reflect-metadata";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import { buildSchema } from "type-graphql";
import { createConnection } from "typeorm";
import { UserResolver } from "./resolvers/UserResolver";
import { TeamResolver } from "./resolvers/TeamResolver";
import cors from "cors";

import { TypeormStore } from "connect-typeorm";
import ExpressSession from "express-session";
import { Session } from "./entity/Session";
import { TaskResolver } from "./resolvers/TaskResolver";
import { EstimationResolver } from "./resolvers/EstimationResolver";

async function main() {
  const connection = await createConnection();
  const app = express();

  const sessionRepository = connection.getRepository(Session);

  app.set("trust proxy", 1);

  app.use(
    ExpressSession({
      name: "teuid",
      resave: false,
      saveUninitialized: false,
      store: new TypeormStore({
        cleanupLimit: 2,
      }).connect(sessionRepository),
      secret: "e9gnligrtln",
      cookie: {
        path: "/",
        domain: undefined,
        sameSite: "lax",
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10,
        httpOnly: false,
        secure: false,
      },
    })
  );

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [UserResolver, TeamResolver, TaskResolver, EstimationResolver],
      validate: false,
    }),
    context: ({ req, res }) => ({ em: connection.manager, req, res }),
  });
  console.log(process.env.SERVER);
  await apolloServer.start();
  apolloServer.applyMiddleware({
    app,
    cors: {
      origin: [
        "http://127.0.0.1:3000",
        "http://localhost:3000",
        "http://localhost:4000",
        "http://127.0.0.1:4000",
        "https://studio.apollographql.com",
        "http://84.38.184.16:3000",
        "http://84.38.184.16:4000",
        "http://84.38.184.16:5000",
        "http://kood.cf:3000",
        "http://kood.cf:4000",
        "http://kood.cf:5000",
      ],
      credentials: true,
    },
  });
  app.listen(4000, () => console.log("Listening on port 4000"));
}

main().catch((error) => console.log(error));
