import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, CreateDateColumn } from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { Task } from "./Task";
import { User } from "./User";

@ObjectType()
@Entity()
export class Estimation {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({nullable:true})
  stage: number;

  @Field()
  @Column()
  complexity: number;

  @Field()
  @Column()
  capacity: number;

  @Field()
  @Column()
  uncertainty: number;

  @Field()
  @Column()
  storypoints: number;

  @Field(() => Task, { nullable: true })
  @ManyToOne(() => Task, (task) => task.id, { nullable: true })
  task: Task;

  @Field(() => User, { nullable: true })
  @ManyToOne(() => User, (user) => user.id, { nullable: true })
  user: User;

  @Field(() => String)
  @CreateDateColumn()
  creationDate: Date;
}
