import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { User } from "./User";
import { Task } from "./Task";

@ObjectType()
@Entity()
export class Team {
    
    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    name: string;

    @Field(() => [User], {nullable: true})
    @OneToMany(type => User, user => user.id) 
    users: User[];

    @Field(() => [Task], {nullable: true})
    @OneToMany(() => Task, task => task.id) 
    tasks: Task[];

}