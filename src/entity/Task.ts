import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { Team } from "./Team";

@ObjectType()
@Entity()
export class Task {
    
    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    name: string;

    @Field({nullable: true})
    @Column({nullable: true})
    estimation: number;

    @Field({nullable: true})
    @Column({nullable: true})
    result: number;

    @Field({nullable: true})
    @Column({nullable: true})
    state: number;

    @Field(() => Team,{nullable: true})
    @ManyToOne(() => Team, team => team.id, {nullable: true})
    team: Team;

}