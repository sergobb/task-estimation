import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { Team } from "./Team";

@ObjectType()
@Entity()
export class User {
    
    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    firstName: string;

    @Field()
    @Column()
    lastName: string;

    @Field()
    @Column({unique:true})
    email: string;

    @Column()
    password: string;

    @Field(() => Team,{nullable: true})
    @ManyToOne(() => Team, team => team.id, {nullable: true})
    team: Team;

}
