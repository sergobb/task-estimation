import { Field, InputType } from "type-graphql";

@InputType()
export class SelectTeamInput {

    @Field()
    userId: number;

    @Field()
    teamId: number;

}