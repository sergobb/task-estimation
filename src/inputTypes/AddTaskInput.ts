import { Field, InputType } from "type-graphql";

@InputType()
export class AddTaskInput {
    
    @Field()
    name: string;

    @Field()
    team: number;

}