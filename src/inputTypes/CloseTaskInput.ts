import { Field, InputType } from "type-graphql";

@InputType()
export class CloseTaskInput {
    @Field()
    id: number;

    @Field()
    result: number;

}