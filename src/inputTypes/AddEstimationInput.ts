import { Field, InputType } from "type-graphql";

@InputType()
export class AddEstimationInput {

    @Field()
    stage: number;

    @Field()
    complexity: number;

    @Field()
    capacity: number;

    @Field()
    uncertainty: number;

    @Field()
    storypoints: number;

    @Field()
    task: number;

    @Field()
    user: number;

}