import { Field, InputType } from "type-graphql";

@InputType()
export class GetTaskInput {

    @Field({nullable: true})
    id: number;
 
    @Field({nullable: true})
    team: number;

    @Field({nullable: true})
    state: number;
}