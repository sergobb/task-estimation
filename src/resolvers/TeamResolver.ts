import { GraphQLResolveInfo } from "graphql";
import { Resolver, Query, Ctx, Mutation, Arg, Int, Info } from "type-graphql";
import { Task } from "../entity/Task";
import { Team } from "../entity/Team";
import { User } from "../entity/User";
import { AddTeamInput } from "../inputTypes/AddTeamInput";
import { GetTeamInput } from "../inputTypes/getTeamInput";
import { ContextType } from "../types";

@Resolver()
export class TeamResolver {
  @Query(() => [Team])
  getAllTeams(@Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<Team[]> {
    return em.getRepository(Team).find();
  }

  @Query(() => Team)
  async getTeamByUser(@Arg("data") data: GetTeamInput, @Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<Team> {
    const user = await em.findOne(User, { id: data.id }, { relations: ["team"] });
    const team = user.team;
    const tasks = await em.find(Task, { team });
    const users = await em.find(User, { team });
    team.tasks = tasks;
    team.users = users;
    return team;
  }

  @Query(() => Team)
  async getTeamById(@Arg("data") data: GetTeamInput, @Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<Team> {
    const team = await em.findOne(Team, { id: data.id });
    const tasks = await em.find(Task, { team });
    const users = await em.find(User, { team });
    team.tasks = tasks;
    team.users = users;
    return team;
  }

  @Mutation(() => Team)
  async addTeam(@Arg("data") data: AddTeamInput, @Ctx() { em }: ContextType): Promise<Team> {
    const team = new Team();
    team.name = data.name;
    return em.save(team);
  }
}
