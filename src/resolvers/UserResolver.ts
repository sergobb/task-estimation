import { GraphQLResolveInfo } from "graphql";
import { Resolver, Query, Ctx, Mutation, Arg, Int, Info } from "type-graphql";
import argon2 from "argon2";
import { User } from "../entity/User";
import { AddUserInput } from "../inputTypes/AddUserInput";
import { ContextType } from "../types";
import { LoginUserInput } from "../inputTypes/LoginUserInput";
import { SelectTeamInput } from "../inputTypes/SelectTeamInput";
import { Team } from "../entity/Team";

@Resolver()
export class UserResolver {
  @Query(() => [User])
  getAllUsers(@Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<User[]> {
    return em.getRepository(User).find();
  }

  @Mutation(() => User)
  async register(@Arg("data") data: AddUserInput, @Ctx() { em, req }: ContextType): Promise<User> {
    const hashedPassword = await argon2.hash(data.password);
    const user = new User();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.email = data.email;
    user.password = hashedPassword;
    const addedUser: User = await em.save<User>(user);
    req.session.userId = addedUser.id;
    return addedUser;
  }

  @Mutation(() => User)
  async login(@Arg("data") data: LoginUserInput, @Ctx() { em, req }: ContextType): Promise<User> {
    const hashedPassword = await argon2.hash(data.password);
    const user: User = await em.findOne(User, { email: data.email });
    if (!user) {
      return null;
    }
    req.session.userId = user.id;
    return user;
  }

  @Mutation(() => User)
  async selectTeam(@Arg("data") data: SelectTeamInput, @Ctx() { em, req }: ContextType): Promise<User> {
    const user: User = await em.findOne(User, { id: data.userId });
    const team: Team = await em.findOne(Team, { id: data.teamId });
    user.team = team;
    return em.save(user);
  }
}
