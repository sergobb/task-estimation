import { GraphQLResolveInfo } from "graphql";
import { Resolver, Query, Ctx, Mutation, Arg, Int, Info } from "type-graphql";
import { Task } from "../entity/Task";
import { Team } from "../entity/Team";
import { AddTaskInput } from "../inputTypes/AddTaskInput";
import { CloseTaskInput } from "../inputTypes/CloseTaskInput";
import { GetTaskInput } from "../inputTypes/getTasksInput";
import { ContextType } from "../types";

@Resolver()
export class TaskResolver {
  @Query(() => [Task])
  getAllTasks(@Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<Task[]> {
    return em.getRepository(Task).find();
  }

  @Query(() => [Task])
  getTasks(@Arg("data") data: GetTaskInput, @Ctx() { em }: ContextType, @Info() info: GraphQLResolveInfo): Promise<Task[]> {
    if (data.id) {
      return em.getRepository(Task).find({ where: { id: data.id } });
    } else {
      return em.getRepository(Task).find({ where: { state: data.state, team: data.team } });
    }
  }

  @Mutation(() => Task)
  async addTask(@Arg("data") data: AddTaskInput, @Ctx() { em }: ContextType): Promise<Task> {
    const team = await em.getRepository(Team).findOne({ where: { id: data.team } });
    const task = new Task();
    task.name = data.name;
    task.team = team;
    task.estimation = 0;
    task.result = 0;
    task.state = 0;
    return em.save(task);
  }

  @Mutation(() => Boolean)
  async closeTask(@Arg("data") data: CloseTaskInput, @Ctx() { em }: ContextType): Promise<boolean> {
    em.update(Task,data.id,{result: data.result, state: 2} );
    return true;
  }  
}
