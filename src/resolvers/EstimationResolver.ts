import { length } from "class-validator";
import { Resolver, Query, Ctx, Mutation, Arg, Int, Info } from "type-graphql";
import { EntityManager } from "typeorm";
import { Estimation } from "../entity/Estimation";
import { Task } from "../entity/Task";
import { Team } from "../entity/Team";
import { User } from "../entity/User";
import { AddEstimationInput } from "../inputTypes/AddEstimationInput";
import { ContextType } from "../types";

@Resolver()
export class EstimationResolver {

  @Mutation(() => Estimation)
  async addEstimation(@Arg("data") data: AddEstimationInput, @Ctx() { em }: ContextType): Promise<Estimation> {
    const task = await em.getRepository(Task).findOne({ where: { id: data.task } });
    const user = await em.getRepository(User).findOne({ where: { id: data.user } });
    const estimation = new Estimation();
    estimation.user = user;
    estimation.task = task;
    estimation.stage = data.stage;
    estimation.complexity = data.complexity;
    estimation.capacity = data.capacity;
    estimation.uncertainty = data.uncertainty;
    estimation.storypoints = data.storypoints;
    // это заглушка, то есть только один член команды
    task.estimation = data.storypoints;
    task.state = 1;
    await em.save(task)
    // -----------------------------
    return em.save(estimation);
  }

  async checkAndFinish(userId: number, taskId: number, em: EntityManager) {
    const user = await em.findOne(User, { id: userId }, { relations: ["team"] });
    const team = user.team;
    const task = await em.findOne(Task, { id: taskId });
    const users = await em.find(User, { team });
    // тут надо переделать, ибо не учитывается количество оценок выстаыленных каждым челном команды
    const estimations = await em.find(Estimation, {task: task, user: user});
    // ----------------
    if (estimations.length === users.length) {          
        const numbers = estimations.map(e => e.storypoints);
        let stop = true;
        for (let i=1; i<numbers.length; i+=1) {
            if(numbers[i]!=numbers[i-1]) {
                stop = false;
                break;
            }
        }
        if (stop) {
            task.state = 1;
            task.estimation = numbers[0];
            await em.save(task);
        }
    }
  }
}
