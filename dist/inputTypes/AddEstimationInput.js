"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddEstimationInput = void 0;
var type_graphql_1 = require("type-graphql");
var AddEstimationInput = /** @class */ (function () {
    function AddEstimationInput() {
    }
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "stage", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "complexity", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "capacity", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "uncertainty", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "storypoints", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "task", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        __metadata("design:type", Number)
    ], AddEstimationInput.prototype, "user", void 0);
    AddEstimationInput = __decorate([
        (0, type_graphql_1.InputType)()
    ], AddEstimationInput);
    return AddEstimationInput;
}());
exports.AddEstimationInput = AddEstimationInput;
//# sourceMappingURL=AddEstimationInput.js.map