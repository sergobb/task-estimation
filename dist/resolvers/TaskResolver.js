"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskResolver = void 0;
var type_graphql_1 = require("type-graphql");
var Task_1 = require("../entity/Task");
var Team_1 = require("../entity/Team");
var AddTaskInput_1 = require("../inputTypes/AddTaskInput");
var CloseTaskInput_1 = require("../inputTypes/CloseTaskInput");
var getTasksInput_1 = require("../inputTypes/getTasksInput");
var TaskResolver = /** @class */ (function () {
    function TaskResolver() {
    }
    TaskResolver.prototype.getAllTasks = function (_a, info) {
        var em = _a.em;
        return em.getRepository(Task_1.Task).find();
    };
    TaskResolver.prototype.getTasks = function (data, _a, info) {
        var em = _a.em;
        if (data.id) {
            return em.getRepository(Task_1.Task).find({ where: { id: data.id } });
        }
        else {
            return em.getRepository(Task_1.Task).find({ where: { state: data.state, team: data.team } });
        }
    };
    TaskResolver.prototype.addTask = function (data, _a) {
        var em = _a.em;
        return __awaiter(this, void 0, void 0, function () {
            var team, task;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, em.getRepository(Team_1.Team).findOne({ where: { id: data.team } })];
                    case 1:
                        team = _b.sent();
                        task = new Task_1.Task();
                        task.name = data.name;
                        task.team = team;
                        task.estimation = 0;
                        task.result = 0;
                        task.state = 0;
                        return [2 /*return*/, em.save(task)];
                }
            });
        });
    };
    TaskResolver.prototype.closeTask = function (data, _a) {
        var em = _a.em;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                em.update(Task_1.Task, data.id, { result: data.result, state: 2 });
                return [2 /*return*/, true];
            });
        });
    };
    __decorate([
        (0, type_graphql_1.Query)(function () { return [Task_1.Task]; }),
        __param(0, (0, type_graphql_1.Ctx)()),
        __param(1, (0, type_graphql_1.Info)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], TaskResolver.prototype, "getAllTasks", null);
    __decorate([
        (0, type_graphql_1.Query)(function () { return [Task_1.Task]; }),
        __param(0, (0, type_graphql_1.Arg)("data")),
        __param(1, (0, type_graphql_1.Ctx)()),
        __param(2, (0, type_graphql_1.Info)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [getTasksInput_1.GetTaskInput, Object, Object]),
        __metadata("design:returntype", Promise)
    ], TaskResolver.prototype, "getTasks", null);
    __decorate([
        (0, type_graphql_1.Mutation)(function () { return Task_1.Task; }),
        __param(0, (0, type_graphql_1.Arg)("data")),
        __param(1, (0, type_graphql_1.Ctx)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [AddTaskInput_1.AddTaskInput, Object]),
        __metadata("design:returntype", Promise)
    ], TaskResolver.prototype, "addTask", null);
    __decorate([
        (0, type_graphql_1.Mutation)(function () { return Boolean; }),
        __param(0, (0, type_graphql_1.Arg)("data")),
        __param(1, (0, type_graphql_1.Ctx)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [CloseTaskInput_1.CloseTaskInput, Object]),
        __metadata("design:returntype", Promise)
    ], TaskResolver.prototype, "closeTask", null);
    TaskResolver = __decorate([
        (0, type_graphql_1.Resolver)()
    ], TaskResolver);
    return TaskResolver;
}());
exports.TaskResolver = TaskResolver;
//# sourceMappingURL=TaskResolver.js.map