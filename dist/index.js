"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var apollo_server_express_1 = require("apollo-server-express");
var express_1 = __importDefault(require("express"));
var type_graphql_1 = require("type-graphql");
var typeorm_1 = require("typeorm");
var UserResolver_1 = require("./resolvers/UserResolver");
var TeamResolver_1 = require("./resolvers/TeamResolver");
var connect_typeorm_1 = require("connect-typeorm");
var express_session_1 = __importDefault(require("express-session"));
var Session_1 = require("./entity/Session");
var TaskResolver_1 = require("./resolvers/TaskResolver");
var EstimationResolver_1 = require("./resolvers/EstimationResolver");
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var connection, app, sessionRepository, apolloServer, _a;
        var _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0: return [4 /*yield*/, (0, typeorm_1.createConnection)()];
                case 1:
                    connection = _c.sent();
                    app = (0, express_1.default)();
                    sessionRepository = connection.getRepository(Session_1.Session);
                    app.set("trust proxy", 1);
                    app.use((0, express_session_1.default)({
                        name: "teuid",
                        resave: false,
                        saveUninitialized: false,
                        store: new connect_typeorm_1.TypeormStore({
                            cleanupLimit: 2,
                        }).connect(sessionRepository),
                        secret: "e9gnligrtln",
                        cookie: {
                            path: "/",
                            domain: undefined,
                            sameSite: "lax",
                            maxAge: 1000 * 60 * 60 * 24 * 365 * 10,
                            httpOnly: false,
                            secure: false,
                        },
                    }));
                    _a = apollo_server_express_1.ApolloServer.bind;
                    _b = {};
                    return [4 /*yield*/, (0, type_graphql_1.buildSchema)({
                            resolvers: [UserResolver_1.UserResolver, TeamResolver_1.TeamResolver, TaskResolver_1.TaskResolver, EstimationResolver_1.EstimationResolver],
                            validate: false,
                        })];
                case 2:
                    apolloServer = new (_a.apply(apollo_server_express_1.ApolloServer, [void 0, (_b.schema = _c.sent(),
                            _b.context = function (_a) {
                                var req = _a.req, res = _a.res;
                                return ({ em: connection.manager, req: req, res: res });
                            },
                            _b)]))();
                    console.log(process.env.SERVER);
                    return [4 /*yield*/, apolloServer.start()];
                case 3:
                    _c.sent();
                    apolloServer.applyMiddleware({
                        app: app,
                        cors: {
                            origin: [
                                "http://127.0.0.1:3000",
                                "http://localhost:3000",
                                "http://localhost:4000",
                                "http://127.0.0.1:4000",
                                "https://studio.apollographql.com",
                                "http://84.38.184.16:3000",
                                "http://84.38.184.16:4000",
                                "http://84.38.184.16:5000",
                                "http://kood.cf:3000",
                                "http://kood.cf:4000",
                                "http://kood.cf:5000",
                            ],
                            credentials: true,
                        },
                    });
                    app.listen(4000, function () { return console.log("Listening on port 4000"); });
                    return [2 /*return*/];
            }
        });
    });
}
main().catch(function (error) { return console.log(error); });
//# sourceMappingURL=index.js.map