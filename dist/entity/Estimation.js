"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Estimation = void 0;
var typeorm_1 = require("typeorm");
var type_graphql_1 = require("type-graphql");
var Task_1 = require("./Task");
var User_1 = require("./User");
var Estimation = /** @class */ (function () {
    function Estimation() {
    }
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.PrimaryGeneratedColumn)(),
        __metadata("design:type", Number)
    ], Estimation.prototype, "id", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.Column)({ nullable: true }),
        __metadata("design:type", Number)
    ], Estimation.prototype, "stage", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], Estimation.prototype, "complexity", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], Estimation.prototype, "capacity", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], Estimation.prototype, "uncertainty", void 0);
    __decorate([
        (0, type_graphql_1.Field)(),
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], Estimation.prototype, "storypoints", void 0);
    __decorate([
        (0, type_graphql_1.Field)(function () { return Task_1.Task; }, { nullable: true }),
        (0, typeorm_1.ManyToOne)(function () { return Task_1.Task; }, function (task) { return task.id; }, { nullable: true }),
        __metadata("design:type", Task_1.Task)
    ], Estimation.prototype, "task", void 0);
    __decorate([
        (0, type_graphql_1.Field)(function () { return User_1.User; }, { nullable: true }),
        (0, typeorm_1.ManyToOne)(function () { return User_1.User; }, function (user) { return user.id; }, { nullable: true }),
        __metadata("design:type", User_1.User)
    ], Estimation.prototype, "user", void 0);
    __decorate([
        (0, type_graphql_1.Field)(function () { return String; }),
        (0, typeorm_1.CreateDateColumn)(),
        __metadata("design:type", Date)
    ], Estimation.prototype, "creationDate", void 0);
    Estimation = __decorate([
        (0, type_graphql_1.ObjectType)(),
        (0, typeorm_1.Entity)()
    ], Estimation);
    return Estimation;
}());
exports.Estimation = Estimation;
//# sourceMappingURL=Estimation.js.map